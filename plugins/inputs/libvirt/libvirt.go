package libvirt

import (
	"log"
	"time"

	"github.com/influxdata/telegraf"
	"github.com/influxdata/telegraf/plugins/inputs"
	"github.com/libvirt/libvirt-go"
)

const sampleConfig = `
  # specify a libvirt connection uri, see https://libvirt.org/uri.html
  uri = "qemu:///system"
`

type GatheredDomainInfo struct {
	name             string
	uuid             string
	state            string
	vcpus            uint
	netRxBytes       uint64
	netTxBytes       uint64
	blockRdBytes     uint64
	blockWrBytes     uint64
	blockRdReqs      uint64
	blockWrReqs      uint64
	blockFlReqs      uint64
	cpuTimeNs        uint64
	userTimeNs       uint64
	systemTimeNs     uint64
	guestTime        float64
	guestTimePerVCpu float64
	memUsedKb        uint64
	memAvailableKb   uint64
	memUnusedKb      uint64
}

type Libvirt struct {
	Uri      string `toml:"uri"`
	Hostname string `toml:"hostname"`
	conn     *libvirt.Connect
}

func (l *Libvirt) SampleConfig() string {
	return sampleConfig
}

func (l *Libvirt) Description() string {
	return "Read domain data from a libvirt daemon"
}

func (l *Libvirt) Connect() error {
	// Connect
	log.Printf("I! Trying to connect to libvirt.\n")
	conn, err := libvirt.NewConnect(l.Uri)
	if err != nil {
		log.Printf("E! Failed to connect to libvirt %s", err)
		return err
	}
	l.conn = conn

	// Get hostname
	hostname, err := conn.GetHostname()
	if err != nil {
		log.Printf("E! Failed get hostname, error: %s", err)
		return err
	}

	l.Hostname = hostname
	log.Printf("I! Hostname: %s.\n", hostname)

	return nil
}

func (l *Libvirt) Gather(acc telegraf.Accumulator) error {
	log.Printf("I! Starting to gather")

	// Connect to libvirt if connection is not open already.
	if l.conn == nil {
		err := l.Connect()
		if err != nil {
			return err
		}
	}

	domains, err := l.conn.ListAllDomains(libvirt.CONNECT_LIST_DOMAINS_ACTIVE)

	if err != nil {
		log.Printf("E! Failed to get domain list, error: %s", err)
		// Set connection to nil, maybe we need to reconnect.
		l.conn = nil
		return err
	}

	for _, domain := range domains {
		start := time.Now()

		statsDom, err := l.gatherDomain(acc, domain)
		if err != nil {
			log.Printf("E! Failed to gather from domain %+v, error: %+v", domain, err)
		}

		elapsed := time.Since(start)
		if elapsed > 1*time.Second {
			log.Printf("E! Gathering from domain %+v took more than 1 sec: %v, there might be something wrong with the domain.", domain, elapsed)
		}
		err = domain.Free()
		if err != nil {
			log.Printf("E! Freeing domain %+v failed: %+v", domain, err)
		}
		if statsDom != nil {
			err = statsDom.Free()
			if err != nil {
				log.Printf("E! Freeing stats domain %+v failed: %+v", statsDom, err)
			}
		}
	}
	log.Printf("Finished gathering from %d domains", len(domains))

	return nil
}

func (l *Libvirt) gatherDomainInfo(domainInfo *GatheredDomainInfo, domain libvirt.Domain) error {
	info, err := domain.GetInfo()

	if err != nil {
		return err
	}

	domainInfo.state = "unknown"
	switch info.State {
	case libvirt.DOMAIN_NOSTATE:
		domainInfo.state = "no state"
	case libvirt.DOMAIN_RUNNING:
		domainInfo.state = "running"
	case libvirt.DOMAIN_BLOCKED:
		domainInfo.state = "blocked"
	case libvirt.DOMAIN_PAUSED:
		domainInfo.state = "paused"
	case libvirt.DOMAIN_SHUTDOWN:
		domainInfo.state = "shutdown"
	case libvirt.DOMAIN_CRASHED:
		domainInfo.state = "crashed"
	case libvirt.DOMAIN_PMSUSPENDED:
		domainInfo.state = "suspended"
	}

	domainInfo.vcpus = info.NrVirtCpu
	domainInfo.memUsedKb = info.Memory

	return nil
}

func (l *Libvirt) gatherDomainStats(domainInfo *GatheredDomainInfo, domain libvirt.Domain) (*libvirt.Domain, error) {
	var doms = []*libvirt.Domain{&domain}
	stats, err := l.conn.GetAllDomainStats(doms, libvirt.DOMAIN_STATS_BLOCK|libvirt.DOMAIN_STATS_INTERFACE, libvirt.CONNECT_GET_ALL_DOMAINS_STATS_ACTIVE)

	if err != nil {
		return nil, err
	}

	if len(stats) <= 0 {
		uuid, _ := domain.GetUUIDString()
		log.Printf("W! Failed to get stats for domain %s", uuid)
		return nil, nil
	}

	if len(stats[0].Net) >= 1 {
		// Let's assume there's exactly 1 network interface.
		domainInfo.netRxBytes = stats[0].Net[0].RxBytes
		domainInfo.netTxBytes = stats[0].Net[0].TxBytes
	}

	totalBlockReadBytes := uint64(0)
	totalBlockWriteBytes := uint64(0)
	totalBlockReadRequests := uint64(0)
	totalBlockWriteRequests := uint64(0)
	totalBlockFlushRequests := uint64(0)
	for _, block := range stats[0].Block {
		totalBlockReadBytes += block.RdBytes
		totalBlockWriteBytes += block.WrBytes
		totalBlockReadRequests += block.RdReqs
		totalBlockWriteRequests += block.WrReqs
		totalBlockFlushRequests += block.FlReqs
	}
	domainInfo.blockRdBytes = totalBlockReadBytes
	domainInfo.blockWrBytes = totalBlockWriteBytes
	domainInfo.blockRdReqs = totalBlockReadRequests
	domainInfo.blockWrReqs = totalBlockWriteRequests
	domainInfo.blockFlReqs = totalBlockFlushRequests

	return stats[0].Domain, nil
}

func (l *Libvirt) gatherMemoryStats(domainInfo *GatheredDomainInfo, domain libvirt.Domain) error {
	stats, err := domain.MemoryStats(uint32(libvirt.DOMAIN_MEMORY_STAT_LAST), 0)

	if err != nil {
		return err
	}

	for _, stat := range stats {
		switch stat.Tag {
		case int32(libvirt.DOMAIN_MEMORY_STAT_AVAILABLE):
			domainInfo.memAvailableKb = stat.Val
		case int32(libvirt.DOMAIN_MEMORY_STAT_UNUSED):
			domainInfo.memUnusedKb = stat.Val
		}
	}

	return nil
}

func (l *Libvirt) gatherCpuStats(domainInfo *GatheredDomainInfo, domain libvirt.Domain) error {

	cpuStats, err := domain.GetCPUStats(-1, 1, 0)

	if err != nil {
		return err
	}

	domainInfo.cpuTimeNs = cpuStats[0].CpuTime
	domainInfo.userTimeNs = cpuStats[0].UserTime
	domainInfo.systemTimeNs = cpuStats[0].SystemTime

	domainInfo.guestTime = float64(domainInfo.cpuTimeNs-domainInfo.userTimeNs-domainInfo.systemTimeNs) / 1e9
	domainInfo.guestTimePerVCpu = domainInfo.guestTime / float64(domainInfo.vcpus)

	return nil
}

func (l *Libvirt) gatherDomain(acc telegraf.Accumulator, domain libvirt.Domain) (*libvirt.Domain, error) {

	var err error
	var statsDom *libvirt.Domain

	domainInfo := GatheredDomainInfo{}

	// First, get domain name and UUID
	domainInfo.uuid, err = domain.GetUUIDString()

	if err != nil {
		log.Printf("E! ERROR gathering domain UUID: %+v", err)
		return nil, err
	}

	domainInfo.name, err = domain.GetName()

	if err != nil {
		log.Printf("E! ERROR gathering domain name for %s: %+v", domainInfo.uuid, err)
	}

	// Gather data from dominfo
	err = l.gatherDomainInfo(&domainInfo, domain)

	if err != nil {
		log.Printf("E! ERROR gathering domain info for %s: %+v", domainInfo.uuid, err)
	}

	// Gather data from domstats
	statsDom, err = l.gatherDomainStats(&domainInfo, domain)

	if err != nil {
		log.Printf("E! ERROR gathering domain stats for %s: %+v", domainInfo.uuid, err)
	}

	// Gather data from memory stats
	err = l.gatherMemoryStats(&domainInfo, domain)

	if err != nil {
		log.Printf("E! ERROR gathering memory stats for %s: %+v", domainInfo.uuid, err)
	}

	// Gather data from cpu-stats
	err = l.gatherCpuStats(&domainInfo, domain)

	if err != nil {
		log.Printf("E! ERROR gathering CPU stats for %s: %+v", domainInfo.uuid, err)
	}

	// Calculate used memory. Use existing memUsedKb if memAvailableKb does not have any info (Windows, sometimes).
	if domainInfo.memAvailableKb > 0 {
		domainInfo.memUsedKb = domainInfo.memAvailableKb - domainInfo.memUnusedKb
	}

	fields := map[string]interface{}{
		"vms":                 1, // :) Useful for showing data in Chronograf.
		"vcpus":               domainInfo.vcpus,
		"used_memory_kb":      domainInfo.memUsedKb,
		"guest_time":          domainInfo.guestTime,
		"guest_time_per_vcpu": domainInfo.guestTimePerVCpu,
		"net_rx_bytes":        domainInfo.netRxBytes,
		"net_tx_bytes":        domainInfo.netTxBytes,
		"block_rd_bytes":      domainInfo.blockRdBytes,
		"block_wr_bytes":      domainInfo.blockWrBytes,
		"block_rd_requests":   domainInfo.blockRdReqs,
		"block_wr_requests":   domainInfo.blockWrReqs,
		"block_fl_requests":   domainInfo.blockFlReqs,
	}

	tags := map[string]string{
		"domain":   domainInfo.name,
		"instance": domainInfo.uuid,
		"state":    domainInfo.state,
		"hv_name":  l.Hostname,
	}

	// debugging
	//log.Printf("domainInfo: %+v\n", domainInfo)

	acc.AddFields("libvirt", fields, tags)

	return statsDom, nil
}

func init() {
	inputs.Add("libvirt", func() telegraf.Input {
		return &Libvirt{
			Uri:      "qemu:///system",
			conn:     nil,
			Hostname: "",
		}
	})
}
