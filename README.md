# Telegraf libvirt

This is a libvirt input execd plugin for Telegraf.

Libvirt connection is implemented with [libvirt-go](github.com/libvirt/libvirt-go) library.

The code is mostly based on the libvirt input code that Telegraf once had built in.
It is now externalised to not have Telegraf build depending on libvirt headers.

Alternative approach would be to communicate with `virsh` tool. However, connecting to libvirt
via the library is significantly faster.

## Building the plugin

Build the `telegraf-libvirt` binary. Libvirt headers must be installed on the system,
it could be a package like `libvirt-dev` (Debian) or `libvirt-devel` (Fedora).

    go build -o telegraf-libvirt cmd/main.go

## Example usage

Prepare a `plugin.conf` file, an example is provided with the code.

    [[inputs.libvirt]]
    # specify a libvirt connection uri, see https://libvirt.org/uri.html
    uri = "qemu:///system"
    hostname = "useful-in-docker-containers"

Add this to your `telegraf.conf`, referring to the binary and configuration file:

    [[inputs.execd]]
    command = ["/path/to/telegraf-libvirt", "-poll_interval", "5s", "-config", "/path/to/plugin.conf"]
    signal = "none"

## License

MIT License
